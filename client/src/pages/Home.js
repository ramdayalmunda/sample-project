import api from "../common/api.js"
import { useState } from "react"

const Home = () => {

    const [ formData, setFormData ] = useState({ username: "", password: "" });
    function handleFormChange(e, fieldName){
        setFormData( {
            ...formData,
            [fieldName]: e.target.value
        } )
    }
    async function clickHandler(e){
        e.preventDefault()

        console.log('click was hit', formData)
        try{

            let { data } = await api.post("/user/login", formData)
            alert(data.message)
            if ( data.success ){
                console.log('handle code for success here')
            }else{
                console.log('handle code for failure here')
            }
        }catch(err){
            console.log(err)
        }
    }

    return <div>
        <h1>Enter Login Data</h1>
        <form onSubmit={clickHandler}>
            <input placeholder="username" value={formData.username} onChange={(e)=>{ handleFormChange(e, 'username')}}/>
            <input placeholder="password" value={formData.password} onChange={(e)=>{ handleFormChange(e, 'password')}}/>
            <button type="submit">Submit</button>
        </form>
    </div>;
};

export default Home;