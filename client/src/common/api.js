import axios from "axios"

const serverPath = "http://localhost:5170"
const api = axios.create({ baseURL: serverPath})

export default api