const router = require('express').Router()
const userController = require("../controller/user.js")


router.post("/login", userController.login)

module.exports = router