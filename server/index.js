const express = require("express")
const cors = require("cors")
const port = 5170
const app = express()

// Middleware to manage CORS
const whitelist = ['http://localhost:3000'];// add your origin here; these are the origins that can make a successfull hit on the backend server
const corsOptions = {
    origin: function (origin, callback) {
        if (whitelist.includes(origin)) {
            callback(null, true);
        } else {
            callback(new Error('Not allowed by CORS'));
        }
    },
};
app.use(cors(corsOptions));


// Middleware to parse JSON and URL-encoded bodies
app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded


app.use("/user", require("./router/user.js"))

app.listen(port, () => {
    console.log(`server on http://localhost:${port}`)
})