const { SERVER_STATUS } = require("../helper/constants.js");
const MESSAGES = require("../helper/messages.js");

module.exports.login = async function (req, res){
    try{
        console.log('to log in the user:', req.body)
        let responseObj = { success: false, message: MESSAGES.INVALID_CREDENTAILS }
        /**
         * Write the login of this method here.
         * Make hits to the DAO(database access objects here)
         * Modify parameters here
         */


        ////// this is temporary. replace this with actual logic
        if (req.body?.username?.toLowerCase() == 'john' && req.body?.password == 'doe'){
            responseObj.success = true;
            responseObj.message = MESSAGES.LOGGED_IN
        }

        res.status(SERVER_STATUS.OK).json(responseObj)

    }catch(err){
        console.log(err)
        res.status( SERVER_STATUS.INTERNAL_SERVER_ERROR ).json({ err: err, message: MESSAGES.INTERNAL_SERVER_ERROR })
    }
}