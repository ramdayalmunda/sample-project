# Copy the git repository
>git clone https://gitlab.com/ramdayalmunda/sample-project

# Install packages
Go to the the client folder using cd command 
> cd client
> npm install

Go to the the server folder. use > cd.. to go back a directory
> cd ../server
> npm install

# Running Servers
## to the Server running the client React app
Go to the client folder
> npm start
## to run the secure backend Server
Go to the server folder
> npm start